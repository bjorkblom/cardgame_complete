import { Game } from './game';

describe('Game', () => {
  let game: Game;
  let cards: Array<string> = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Kn', 'Da', 'Ku', 'A'];

  beforeEach(() => {
    game = new Game();
  });

  it('should greet player when game starts', () => {
    let date: Date = new Date(2017, 1, 1, 10);
    expect(game.start(date)).toBe('Good morning and welcome to an awesome cardgame');
  });

  it('should say Good morning in the morning', () => {
    let date: Date = new Date(2017, 1, 1, 10);
    expect(game.getGreeting(date)).toBe('Good morning');
  });

  it('should say Good afternoon in the afternoon', () => {
    let date: Date = new Date(2017, 1, 1, 13);
    expect(game.getGreeting(date)).toBe('Good afternoon');
  });

  it('should say Good evening in the evening', () => {
    let date: Date = new Date(2017, 1, 1, 22);
    expect(game.getGreeting(date)).toBe('Good evening');
  });

  it('should have all available cards', () => {
    expect(game.cards).toEqual(cards);
  });

  it('should take one card', () => {
    let card: string = game.takeCard();
    expect(cards).toContain(card);
  });

  it('should take a random card', () => {
    let takenCards: object = {};
    for(let i: number = 0; i < 50000; i++) {
      let card = game.takeCard();
      takenCards[card] = (takenCards[card] || 0) + 1 ;
    }
    expect(Object.keys(takenCards).length).toBeGreaterThan(1);
  })

  it('should be possible to take one of all available cards', () => {
    let takenCards: object = {};
    for(let i: number = 0; i < 50000; i++) {
      let card = game.takeCard();
      takenCards[card] = (takenCards[card] || 0) + 1 ;
    }
    expect(Object.keys(takenCards).length).toBe(13);
  })

  it(`should return 'Invalid card' if card1 is invalid`, () => {
    expect(game.compareCards('1', cards[1])).toBe('Invalid card')
  })

  it(`should return 'Invalid card' if card2 is invalid`, () => {
    expect(game.compareCards(cards[1], 'C')).toBe('Invalid card')
  })

  it(`should return 'Invalid card' if both card1 and card2 is invalid`, () => {
    expect(game.compareCards('1', 'C')).toBe('Invalid card')
  })

  it('should compare cards and return player 1 as winner if player 1 has higher card', () => {
    expect(game.compareCards(cards[1], cards[0])).toBe('Player 1 wins');
  })

  it('should compare cards and return player 2 as winner if player 2 has higher card', () => {
    expect(game.compareCards(cards[4], cards[6])).toBe('Player 2 wins');
  })

  it('should compare cards and return draw if player 1 and 2 has the same card', () => {
    expect(game.compareCards(cards[8], cards[8])).toBe(`It's a draw`);
  })
});
