export class Game {
    cards: Array<string> = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Kn', 'Da', 'Ku', 'A'];

    constructor() {}
    /**
     * Greet the player and starts a new game
     * @param date
     */
    start(date: Date = new Date()) {
        let greeting: string = this.getGreeting(date);
        return `${this.getGreeting(date)} and welcome to an awesome cardgame`;
    }

    /**
     * Returns greeting depending on what time it is
     * @param date
     */
    getGreeting(date: Date = new Date()) {
        let hour = date.getHours();
        if(hour >= 0 && hour < 12) {
            return 'Good morning';
        }
        else if(hour >= 12 && hour < 18) {
            return 'Good afternoon';
        }
        else if(hour >= 18 || hour <= 6) {
            return 'Good evening';
        }
    }

    /**
     * Randomizes and return a card from the deck
     */
    takeCard() {
        let rnd: number = Math.round(Math.random() * 12)
        return this.cards[rnd];
    }

    /**
     * Compare two cards and return who's the winner.
     * @param card1
     * @param card2
     */
    compareCards(card1: string, card2:string) {
        let index1 = this.cards.indexOf(card1);
        let index2 = this.cards.indexOf(card2);
        if(index1 === -1 || index2 === -1) {
            return 'Invalid card';
        }
        if(index1 > index2) {
            return 'Player 1 wins';
        }
        else if(index1 < index2) {
            return 'Player 2 wins';
        }
        return `It's a draw`;
    }
}