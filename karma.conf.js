// Karma configuration
// Generated on Thu May 19 2016 15:34:34 GMT-0400 (EDT)

module.exports = function (config) {
  config.set({
    frameworks: [
      'jasmine',
      'karma-typescript'
    ],
    files: [
      { pattern: 'src/**/*.ts' }
    ],
    plugins: [
      'karma-jasmine',
      'karma-chrome-launcher',
      'karma-typescript'
    ],
    preprocessors: {
      '**/*.ts': ['karma-typescript']
    },
    reporters: ['progress', 'karma-typescript'],
    browsers: ['Chrome'],
    tsconfig: "./tsconfig.json"
  })
};
